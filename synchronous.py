import requests, time
from collections import Counter    

def print_data(data, head=None, amount=10):
    """Print items in given touple with limit."""
    if head: print head
    print "nr\tagent\tamount"
    for i, d in enumerate(data[:amount]):
        print "%s\t%s\t%d" % (i, d[0], d[1])

class FundaSearch(object):
    url_api = 'http://partnerapi.funda.nl/feeds/Aanbod.svc/'
 
    def __init__(self, key):
        self.key = key

    def query_api(self, page=1, pagesize=25, **kw):
        """Simple http getter to make call into funda API.
            Return request response object with data. 
        """
        kw.update({'page':page, 'pagesize': pagesize})
        #Make a GET query and pass all required arguments.
        res = requests.get("%s/json/%s" % (self.url_api, self.key), params=kw)
        #return request object with data that can be evaluated to python dictonary.
        return res

    def get_most_common(self, **kw):
        """Determine which makelaar's have the most object listed for sale.
            Return tuple with makelaar id and number of its occurance.
        """
        page, agent_id = 1, []
        #This is very nasty synchronize blocking way of doing this task.
        #Morover if any error occure on server side it could wait for data forever,
        #but to keep it simple and show how this should not be done I wrote it here.
        while True:    
            res = self.query_api(page, **kw)
            #Check status code, if timeout or not accessible wait 1m.
            if res.status_code != 200:
                print "Error occure, reason: %s" % res.reason
                print "White 1 minute and try again..."
                time.sleep(60)
                continue
            #Evaluate json message to python dictionary object.
            data = res.json()
            #increase the page number.
            page += 1
            #This is again something that I don`t like to much because can allocate huge list object...
            #but nothing that we can not survive this days.
            agent_id += [ x['MakelaarId'] for x in data['Objects'] ]

            if data['Paging']['HuidigePagina'] == data['Paging']['AantalPaginas']:# or data['Paging']['HuidigePagina'] == 4:
                break
        #Use build it module that count for me items in my list.
        c = Counter(agent_id)
        return c.most_common()

if __name__ == '__main__':
    f = FundaSearch('005e7c1d6f6c4f9bacac16760286e3cd')
    data_1 = f.get_most_common(type='koop', zo='/amsterdam/tuin/')
    data_2 = f.get_most_common(type='koop', zo='/amsterdam/')
    print_data(data_1, "Data for /amsterdam/tuin/")
    print_data(data_2, "Data for /amsterdam/")