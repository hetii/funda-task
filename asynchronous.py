from twisted.internet import reactor, defer, task
from twisted.web.client import getPage
from twisted.web.error import Error
from twisted.internet.defer import DeferredList
from collections import Counter 
import urllib, json

class FundaSearch(object):

    url_api = 'http://partnerapi.funda.nl/feeds/Aanbod.svc/'  
 
    def __init__(self, key):
        """Constructor
        """
        self.key = key
        self.agent_ids = []
        self.first = None
        self.pages = 0

    def queryApi(self, page=1, pagesize=25, **kw):
        """Simple http getter to make call into funda API.
            Return deffered response object with chains of callbacks. 
        """
        kw.update({'page':page, 'pagesize': pagesize})
        #Make a GET query and pass all required arguments.
        url = "%s/json/%s/?%s" % (self.url_api, self.key, urllib.urlencode(kw))

        d = getPage(url, method='GET')

        d.addCallback(self.pageCallback, **kw)
        d.addErrback(self.errorHandler, **kw)
        return d
        
    def errorHandler(self, result, **kw):
        """Error handler that are fired if we don`t get properp response from API.
        """
        #Just reshedule task
        d = self.queryApi(**kw)
        pass

    def pageCallback(self, result, **kw):
        """Main method to proccess data from successful callbacks.
        """

        #Transform json data do python dictonary.
        data = json.loads(result)
        #We need first query to determine amount of pages.
        if self.first is None:
            self.first = 1
            # #get amount of all pages for this session.
            self.pages = data['Paging']['AantalPaginas']
            #create other asynchronous task.
            for page_nr in xrange(1, self.pages):
                kw['page'] += 1
                self.queryApi(**kw)
        #here, on any success we decrement amount of pages.
        self.pages-=1
        if self.pages == 0:
            #Usualy DefferdList object is used to determine if all callbacks are done.
            #but we also need to handle errors that need to be resheduled, thats why I don`t use such list.
            #also as an alternative its possible to use L{twisted.internet.task.Cooperator}
            self.printResult("Data for %s" % kw['zo'])
        #Store our ids.
        self.agent_ids += [ x['MakelaarId'] for x in data['Objects'] ]   

    def printResult(self, head=None):
        """Print items in given touple with limit.
        """
        #Use Counter class to count most common object in ourt list.    
        c = Counter(self.agent_ids)
        if head: print head
        print "nr\tagent\tamount"
        for i, d in enumerate(c.most_common()[:10]):
            print "%s\t%s\t%d" % (i, d[0], d[1])
        # Stop reactor at the end of the work.
        #if reactor.running:
        #    reactor.stop()

if __name__ == '__main__':

    s1 = FundaSearch('005e7c1d6f6c4f9bacac16760286e3cd')
    s2 = FundaSearch('005e7c1d6f6c4f9bacac16760286e3cd')
    s1.queryApi(type='koop', zo='/amsterdam/')
    s2.queryApi(type='koop', zo='/amsterdam/tuin/')
    if not reactor.running:
        reactor.run()